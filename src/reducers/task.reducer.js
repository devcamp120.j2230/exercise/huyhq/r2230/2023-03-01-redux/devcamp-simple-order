const taskState = {
  mobileList: [
    {
      name: "IPhone X",
      price: 900,
      quantity: 0
    },
    {
      name: "Samsung S9",
      price: 800,
      quantity: 0
    },
    {
      name: "Nokia 8",
      price: 650,
      quantity: 0
    }
  ],
  totalValue: 0
}

const taskReducer = (state = taskState, action) => {
  switch (action.type) {
    case "BUTTON_CLICK":
      state.mobileList[action.payload].quantity += 1;
      state.totalValue = 0;
      state.mobileList.forEach((value) => {
        state.totalValue += value.price * value.quantity;
      })
      break;
    default:
      break;
  }

  //state lưu giá trị mới
  return { ...state };
};

export default taskReducer;