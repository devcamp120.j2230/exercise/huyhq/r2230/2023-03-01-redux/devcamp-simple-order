import { combineReducers } from "redux";
import { createStore } from "redux";
import taskReducer from "../reducers/task.reducer";
//root chứa các task reducer 
const rootReducer = combineReducers({
    taskReducer
});

//tạo store
const store = createStore(rootReducer);

export default store;
